{
    'name': "Real state",
    'author': "Utsho_Biswas",
    'category': 'CRM',
    'description': '',

    'depends': [ 'base',],
    'data': [
    'security/ir.model.access.csv',
    'views/estate_property_views.xml',
    'views/estate_menus.xml',
    'views/estate_property_types_views.xml',
    'views/estate_types_menu.xml',
    'views/estate_property_tag_view.xml',
    'views/estate_property_offer_action.xml',
    'views/estate_offer_view.xml',



],

    


    'installable': True,
    'application': True,
    'auto_install': False,
    'license' : 'LGPL-3'
}
