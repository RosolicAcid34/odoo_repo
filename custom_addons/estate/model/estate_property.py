from datetime import date
from odoo.exceptions import *

from odoo import models, fields, api
from dateutil.relativedelta import relativedelta
from odoo.tools.float_utils import float_compare

date_of_availability = date.today() + relativedelta(months=+3)


class EstateProperty(models.Model):
    _name = "estate.property"
    _description = "Real Estate details"
    _order = "id desc"

    name = fields.Char(required=True)
    description = fields.Text()
    postcode = fields.Char()
    date_availability = fields.Date(default=date_of_availability, copy=False)
    expected_price = fields.Float(required=True)
    selling_price = fields.Float(readonly=True, copy=False)
    bedrooms = fields.Integer(default=2)
    living_area = fields.Integer()
    facades = fields.Integer()
    garage = fields.Boolean()
    garden = fields.Boolean()
    garden_area = fields.Integer()
    garden_orientation = fields.Selection(
        selection=[('north', 'North'), ('south', 'South'), ('east', 'East'), ('west', 'West')])
    Active = fields.Boolean(required=False)
    state = fields.Selection(
        selection=[('new', 'New'), ('offer received', 'Offer Received'), ('offer accepted', 'Offer Accepted'),
                   ('sold', 'Sold'), ('canceled', 'Canceled')], default='new')

    property_type_id = fields.Many2one("estate.property.types")
    buyer = fields.Many2one("res.partner")
    seller = fields.Many2one("res.partner")
    tag_ids = fields.Many2many("estate.property.tag")
    offer_ids = fields.One2many("estate.property.offer", "property_id")
    total_area = fields.Integer(compute="_compute_total_area")
    best_offer = fields.Float(compute='_compute_best_offer')
    date_of_create = fields.Date(default=date.today(), readonly=True)



    _sql_constraints = [
        ('check_expected_price', 'CHECK(expected_price > 0 )',
         'A property expected price must be strictly positive'),
        (
            'check_selling_price','CHECK(selling_price >= 0)',
            ('A property selling price must be positive')
        )
    ]

    @api.constrains('selling_price')
    def _check_selling_price(self):
        for record in self:
            if record.selling_price != 0 and float_compare(record.selling_price, record.expected_price * 0.9,
                                                           precision_digits=2) <= 0:
                raise ValidationError(
                    "The selling price cannot be lower than 90% of the expected price.")

    @api.onchange("garden")
    def _onchange_garden(self):
        if self.garden == True:
            self.garden_area = 10
            self.garden_orientation = "north"
        else:
            self.garden_area = 0
            self.garden_orientation = ""

    def _inverse_onchange_garden(self):
        self.garden_area = 0
        self.garden_orientation = ""

        return {'warning': {
            'title': ("Warning"),
            'message': ('This option is not supported for Garden Orientation')}}

    @api.depends('garden_area', 'living_area')
    def _compute_total_area(self):
        for properties in self:
            properties.total_area = properties.living_area + properties.garden_area

    @api.depends('offer_ids')
    def _compute_best_offer(self):
        for record in self:
            if record.offer_ids:
                record.best_offer = max(record.offer_ids.mapped('price'))
            else:
                record.best_offer = 0.00

    def sold_button(self):
        if self.state == "canceled":
            raise UserError("Canceled properties cannot be sold!")
        else:
            for record in self:
                record.state = "sold"
            return True

    def cancel_button(self):
        if self.state == "sold":
            raise UserError("Sold properties can't be canceled!")
        else:
            for record in self:
                record.state = "canceled"
            return True


class EstatePropertyTypes(models.Model):
    _name = "estate.property.types"
    _description = "New types"
    _order = "sequence"

    name = fields.Char(required=True)
    property_ids = fields.One2many("estate.property" , 'property_type_id')
    sequence = fields.Integer('Sequence', default=1)
    offer_ids = fields.One2many("estate.property.offer", "type_id")
    offer_count = fields.Integer(compute='_compute_count')

    @api.depends('offer_ids')
    def _compute_count(self):
        self.offer_count = len(self.offer_ids)


_sql_constraints = [
        ('un_name', 'UNIQUE (name)', 'A property type name must be unique!')
    ]



class EstatePropertyTag(models.Model):

    _name = "estate.property.tag"
    _description = "tags of your property!!"
    _order = "name"

    name = fields.Char(required=True)
    color = fields.Integer()
    _sql_constraints = [
        ('unique_name', 'UNIQUE (name)', 'A property tag name must be unique!')
    ]


class EstatePropertyOffer(models.Model):
    _name = "estate.property.offer"
    _description = "Different tags of houses"
    _order = "price desc"

    status = fields.Selection(selection=[('accepted', 'Accepted'), ('refused', 'Refused')], copy=False)
    partner_id = fields.Many2one("res.partner", required=True)
    property_id = fields.Many2one("estate.property", required=True)
    price = fields.Float()

    validity = fields.Integer(default=7)
    date_deadline = fields.Date(default=date.today(), compute='_compute_dead_line', inverse="_inverse_dead_line")
    type_id = fields.Many2one(related='property_id.property_type_id', store=True)

    _sql_constraints = [
        ('check_price', 'CHECK(price > 0 )',
         'An offer price must be strictly positive')
    ]
    @api.depends("property_id.date_of_create", "validity")
    def _compute_dead_line(self):
        for record in self:
            record.date_deadline = record.property_id.date_of_create + relativedelta(days=record.validity)

    def _inverse_dead_line(self):
        for record in self:
            record.validity = (record.date_deadline - record.property_id.date_of_create).days

    def action_accept(self):
        for record in self:
            if record.status == "accepted":
                return True
            else:
                for oid in record.property_id.offer_ids:
                    if oid.status == "accepted":
                        raise UserError("An offer already accepted !")
                record.status = "accepted"
                record.property_id.state = "offer accepted"
                record.property_id.selling_price = record.price
                record.property_id.buyer = record.partner_id

            return True

    def action_refuse(self):
        for record in self:
            if record.status == "refused":
                return True
            else:
                record.status = "refused"
            for oid in record.property_id.offer_ids:
                if oid.status == "refused":
                    record.property_id.selling_price = 0
                    record.property_id.buyer = None
            return True
